import bl00mbox
import random
import time
import math
import leds

from st3m import InputState, Responder
from st3m.application import Application, ApplicationContext
from st3m.goose import Optional
from st3m.utils import xy_from_polar, tau
from st3m.ui.view import ViewManager
from st3m.ui.interactions import CapScrollController
from ctx import Context

class App(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.apath = app_ctx.bundle_path + "/4h0l3.jpg"


    def draw(self, ctx: Context) -> None:
        ctx.save()

        ctx.rgb(0, 0, 0).rectangle(-144, -144, 288, 288).fill()
        ctx.image(self.apath, -121, -121, 240, 240)

        ctx.restore()


if __name__ == "__main__":
    from st3m.run import run_view

    run_view(App(ApplicationContext()))
